#!/usr/bin/python
# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import time
import signal
import argparse
import logging
import threading
#-- custom libraries
import vlbimon     #-- interaction with VLBImonitor server
import sitedata    #-- site data (site-specific)

#-- TODO
# * allow parameter json value 'null'

def main():
    """Parse command line arguments and start client"""
    #-- parse command line args{{{
    parser = argparse.ArgumentParser()
    parser.add_argument("--tcollect", type=int, default=2, help="data collection interval [s]")
    parser.add_argument("--loglevel","-l", default="INFO", choices=["DEBUG", "INFO", "WARNING"], help="log level")
    args = parser.parse_args()

    #-- setup logger
    logger = logging.getLogger()
    formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s", datefmt="%Y-%m-%dT%H:%M:%SZ")
    logging.Formatter.converter = time.gmtime
    #-- base loglevel, acts as floor for attached handlers
    logger.setLevel(logging.DEBUG)
    #-- add file and console handlers
    fh = logging.FileHandler('client.log')
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)
    ch = logging.StreamHandler()
    ch.setLevel(args.loglevel)
    #ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)

    logging.info('starting client...')

    client = Client(args.tcollect)
    client.run()
#}}}


class Client(object):
    """VLBImonitor client class"""
    def __init__(self, tcollect):#{{{
        #-- settings
        self.tcollect = tcollect

        #-- parameter definitions from masterlist and parameter.map
        self.pardef = vlbimon.ParameterDef()
        self.pardef.warnMissingSiteParameters()

        #-- privacy mode switch
        possible_modes = set([k for d in self.pardef.parlist.values() for k in d['cadence'].keys()])
        privacymodeswitch = vlbimon.PrivacyModeSwitch(possible_modes)

        #-- instantiate records cache
        self.records = vlbimon.Records()
        #-- start record submitter threads
        self.submitters = vlbimon.Submitters(self.records.queues)

        #-- create fresh dictionary for parameter [x,y] data
        #-- recreate after submission
        self.datastore = vlbimon.Datastore(self.pardef.parlist, privacymodeswitch.get)

        #-- siteData getter instance looks up datatype and cadence
        try:
            self.siteData = sitedata.Sitedata(self.pardef.site_parlist)
        #-- wrap the old interface
        except AttributeError:
            self.siteData = sitedata.Getter(self.pardef.site_parlist)
            def collect():
                self.siteData.update_values()
                return self.siteData.params
            setattr(self.siteData, 'collect', collect)

        #-- timing, share between iterations
        self.t = time.time()  #-- beginning of cycle
        self.cancel = threading.Event()
        self.finished = threading.Event()

        #-- cleanly terminate main loop when sig is received
        signal.signal(signal.SIGINT, self.close)
        signal.signal(signal.SIGTERM, self.close)


    def run(self):
        """Main loop"""
        while not self.cancel.is_set():#{{{
            try:
                self.main()
            except Exception as e:
                logging.exception('--- EXCEPTION ---')
                logging.exception(e)
                logging.exception('')
                #-- rate limit
                self.cancel.wait(1)
        else:
            self.finished.wait()
#}}}

    def close(self, signum, frame):
        """Close sitedata and terminate main loop"""
        logging.warning('signal %s detected, closing', signum)#{{{
        self.cancel.set()
        try:
            self.siteData.close(signum, frame)
        except: pass
        self.submitters.close(signum, frame)
        self.finished.set()
    #}}}

    def main(self):
        """Collect data and submit if due."""
        #-- collect data from site{{{
        data_site = self.siteData.collect()

        #-- insert
        self.datastore.insert(data_site)

        #-- push data to records cache
        if time.time() > self.datastore.t_due:
            data = self.datastore.get()
            self.records.put(data)

            #-- clean up for next iter
            self.datastore.clear()

            #-- reinit datastore if pardef got updated
            if self.pardef.readNew() is not None:
                self.datastore.reset(self.pardef.parlist)

        #-- sleep
        tnow = time.time()
        self.t = max(tnow, self.t + self.tcollect)  #-- time at beginning of cycle in main loop
        self.cancel.wait(self.t - tnow)
#}}}
#}}}


#-- driver
if __name__ == "__main__":
    main()

# vim: foldmethod=marker
