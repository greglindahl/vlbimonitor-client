#!/usr/bin/python2
# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function
import requests

#-- server settings
SERVER = "https://vlbimon1.science.ru.nl"

#-- facility name
FACILITY = 'mytelescope'

##-- Authentication methods:
##   1. username/password
##   2. IP address whitelisting
#
#import getpass
#USERNAME = 'me'
#password = getpass.getpass("Password for \"{:}\":\n".format(USERNAME))

#-- create the data dict
#   - key names and their purpose are defined in the masterlist
#   - value is a list of (time, value) tuples
#       - time at which value was measured in seconds since 1970-01-01 UTC
#       - value datatype and units are defined in the masterlist
data = {
    # measured 'false' at 2017-04-04T08:00:00Z
    # measured  'true' at 2017-04-04T08:02:00Z
    "telescope:onSource": [(1491292800, False), (1491292920, True)],
    # measured '315 K' at 2017-04-04T08:01:00Z
    "if:1:systemTemp": [(1491292860, 315)],
    }

url = "{:}/raw/{:}".format(SERVER, FACILITY)

#-- submit using BasicAuth
if 'USERNAME' in locals():
    r = requests.post(url, json=data, auth=(USERNAME, password))
#-- submit from whitelisted IP address
else:
    r = requests.post(url, json=data)

#-- print response
print(r.status_code, r.reason)
if r.status_code != requests.codes.ok:
    print(r.text)
