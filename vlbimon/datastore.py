# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import time
import collections
import logging

#-- document datapoint [x,y] with y==None means no value, skip

class Datastore():
    """Hold data indexed by formal parnames."""
    #-- rate limit point insertion
    RATELIMIT = 1 #-- seconds
    #-- let the server know there is no data
    HEARTBEAT = 600 #-- seconds
    def __init__(self, parlist, get_privacymode):
        self.parlist = parlist
        self.get_privacymode = get_privacymode
        self.data = {}
        self.t_due = 0
        self.due_reasons = []
        #-- keep track of changes in parameter (x,y) values
        self.dataLast = {}
        #-- delayed insertion
        self.dataPending = {}
        #-- initialize
        self.reset(parlist)

    def get(self):
        """Return subset of self.data that has non-empty values"""
        data = collections.OrderedDict()
        for k in self.parlist:
            if len(self.data[k]) == 0: continue
            data[k] = self.data[k]
        logging.debug("datastore.get: t_due reason: %s", str(self.due_reasons))
        return data

    def reset(self, parlist):
        """reinit data with keys from parlist"""
        self.parlist = parlist
        self.data.clear()
        for k in parlist:
            self.data[k] = []
        return self.data

    def clear(self):
        """clear data"""
        self.t_due = time.time() + self.HEARTBEAT
        self.due_reasons = ['HEARTBEAT']
        for k in self.data:
            self.data[k] = []
        return self.data

    def __insert1(self, key, point, cadence):
        """Insert a single [x,y] point and set the time at which this point is due for submission.
        Parameters that are not immediately due may be packed together."""
        #-- submission due time
        if cadence==0:
            t_due = 0
        #elif key in self.dataLast:
        #    t_due = self.dataLast[key][0] + 1.5*cadence
        #else:
        #    t_due = point[0] + .5*cadence
        #-- grant time to bundle regardless of when this or the previous value was measured
        #-- submission of measurement parameters is not time critical
        else:
            t_due = self.t_now + 1.5*cadence
        #-- set submission due time and keep track of reason
        if t_due == self.t_due:
            self.due_reasons.append(key)
        elif t_due < self.t_due:
            self.due_reasons = [key]
            self.t_due = t_due
        #-- insert
        self.data[key].append(point)
        self.dataLast[key] = point
        self.dataPending.pop(key, None)

    def insert(self, data_site):
        """Insert data points from data_site into data.
        data_site is indexed by site-dependent parname_site,
        data is indexed by VLBImonitor standardized parname."""
        self.t_now = time.time()
        cadencemode = self.get_privacymode()
        #-- insert new time-value points{{{
        #-- skip duplicate points and limit rate to cadence
        siteDataPars = set(data_site.keys())
        x_max = 0
        for k, props in self.parlist.items():
            #-- skip if parameter is not linked to the site
            if 'parname_site' not in props: continue
            parname_site = props['parname_site']
            #-- data point
            if not parname_site in data_site: continue
            #-- deal with this parname_site and delete it from the dict
            points = data_site[parname_site]
            siteDataPars.discard(parname_site)

            #-- validity check
            #-- invalid datapoints
            if points is None: continue #-- silently skip no-data
            if not isinstance(points, list):
                logging.warning("insert_sitedata: input not list of data points, skipping: %s", parname_site)
                continue

            cadence = self.parlist[k]['cadence'].get(cadencemode, None)
            #-- this parameter is not included in the current privacy mode
            if cadence is None: continue

            #-- insert datapoints
            for p in points:
                #-- check datapoint
                if p is None: continue #-- silently skip no-data
                if not isinstance(p, list)  or  len(p) != 2:
                    logging.warning("insert_sitedata: datapoint not in [x,y] format, skipping: %s", parname_site)
                    logging.debug("%s", str(p))
                    continue

                #-- check values in datapoint
                if p[1] is None: continue #-- silently skip no-data p
                if not isinstance(p[0], (int, float))  or  not isinstance(p[1], (int, float, str)):
                    logging.warning("insert_sitedata: datapoint [x,y] coordinates have invalid datatype, skipping: %s", parname_site)
                    logging.debug("%s", str(p))
                    continue

                if p[0] <= 0:
                    logging.warning("insert_sitedata: datapoint [x,y] with x<=0, skipping: %s", parname_site)
                    logging.debug("%s", str(p))
                    continue

                #-- sitedata's current parse time
                x_max = max(p[0], x_max)

                #-- limit time precision to decisecond
                if isinstance(p[0], float): p[0] = float("%.1f" % p[0])

                #-- reject p sometimes
                if k in self.dataLast:
                    pointLast = self.dataLast[k]

                    #-- rule 0: this point was inserted before
                    if p == pointLast: continue

                    xsince = p[0] - pointLast[0]
                    #-- warn about historic data
                    if xsince < 0:
                        logging.warning("datapoint older than previous: %s, %ds", k, xsince)

                    #-- rule 1: rate limit
                    if xsince <= self.RATELIMIT:
                        #-- skip
                        if p[1] == pointLast[1]: continue
                        #-- defer
                        self.dataPending[k] = p
                        continue

                    #-- rule 2: constant value caused by no state change or no measurement done
                    if p[1] == pointLast[1]:
                        #-- state parameter and no change: skip
                        if cadence == 0: continue
                        #-- assume only state parameters use strings, don't rely on the cadence to be set correctly
                        if isinstance(p[1], str): continue
                        #-- measurement parameter and no change:
                        #-- a new measurement on a continuous domain will not yield the same value, so no measurement was done
                        if isinstance(p[1], float): continue
                        #-- measurements on a discrete domain should be rare
                        #-- i.e. int parameters will typically have zero cadence

                    #logging.debug('point passed criteria: %s, new:%s, last:%s', k, str(p), str(pointLast))

                    #-- defer to meet requested cadence
                    if xsince <= cadence:
                        self.dataPending[k] = p
                        continue
                    #-- else: insert

                #-- insert a time-value point
                self.__insert1(k, p, cadence)

        #-- insert pending datapoints
        for k in list(self.dataPending.keys()):
            p = self.dataPending[k]

            cadence = self.parlist[k]['cadence'].get(cadencemode, None)
            #-- this parameter is not included in the current privacy mode
            if cadence is None: continue

            xsince = x_max - self.dataLast[k][0]
            if xsince > cadence:
                self.__insert1(k, p, cadence)

        if len(siteDataPars) > 0:
            parign = str(siteDataPars)
            logging.warning("WARNING: some sitedata is ignored, params are not associated in parameter.map: %s", parign)
#}}}

# vim: foldmethod=marker
