# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import re
import logging
import threading
import subprocess
try:
    from urllib.parse import urljoin  #-- python3
except ImportError:
    from urlparse import urljoin  #-- python2
try:
    import queue  #-- python3
except ImportError:
    import Queue as queue  #-- python2
#-- 3rd-party libraries
import requests
import json
#-- custom libraries
from . import config  #-- vlbimon configuration file

#-- max number of seconds before submission is retried after an error occured
TSLEEP_MAX = 60
TSLEEP = lambda n: min(2**(min(n*.5, 20)), TSLEEP_MAX)  # prevent overflow

def counter():
    """Consecutive state occurrence counter"""
    counter.state, counter.n = -1, 0#{{{
    def count(state):
        if state == counter.state:
            counter.n += 1
        else:
            counter.n = 0
            counter.state = state
        return counter.n
    return count
#}}}

class Session(requests.Session):
    """Split request url into server + endpoint"""
    def __init__(self, prefix_url=None, *args, **kwargs):
        super(Session, self).__init__(*args, **kwargs)
        self.prefix_url = prefix_url

    def post(self, url, *args, **kwargs):
        url = urljoin(self.prefix_url, url)
        return super(Session, self).post(url, *args, **kwargs)

    def request(self, method, url, *args, **kwargs):
        url = urljoin(self.prefix_url, url)
        return super(Session, self).request(method, url, *args, **kwargs)


class Submitters():
    """Manage a bunch of Submitter threads"""
    def __init__(self, queues):#{{{
        self.queues = queues

        clientversion = self.__mkversion()
        logging.info('client version: %s', clientversion)

        #-- start submitter threads for each queue
        self.threads = []
        for i,q in enumerate(self.queues):
            t = Submitter(q, i, q.server, clientversion)
            t.start()
            self.threads.append(t)

    def close(self, signum, frame):
        for t in self.threads:#{{{
            t.close()
        for t in self.threads:
            t.join()


    def __mkversion(self):
        """Read the client code version from git or from a .version file"""
        #-- obtain the version from git
        try:
            cmd = "git --git-dir={:}/.git describe --abbrev=4 --dirty".format(config.rootdir)
            return subprocess.check_output(cmd.split(), universal_newlines=True).strip()
        except: pass

        #-- Read the version string from file when running the client
        #-- outside the source tree or git is not available at runtime.
        #-- Save the version string to file during install:
        #-- # git describe --abbrev=4 --dirty > .version
        try:
            fname = config.rootdir + '/.version'
            with open(fname, 'r') as f:
                return f.readline().strip()
        except: pass
#}}}


class Submitter(threading.Thread):
    """Submit items from a records queue to a server"""
    #-- detect schema errors in the server response
    SERVER_SCHEMA_ERROR_REGEX = r'\bdata\.([a-zA-Z0-9:]+)(\.[0-9]+\.[01])?: '

    class SubmissionError(Exception): pass

#{{{
    def __init__(self, qin, iserver, server, clientversion):
        super(Submitter, self).__init__()#{{{

        #-- queue backup file
        username = config.username.replace(' ', '_')
        self.fname = config.storagedir + "/{:}.{:}".format(config.facility, username) + ".queue" # date

        self.qin = qin
        #-- use separate output queue to enable putting records back on the stack upon failed submission
        self.qout = queue.LifoQueue()
        self.iserver = iserver
        self.irecord = 0
        #-- start with configured proxies to begin with
        #-- the proxies are dropped if they fail
        self.proxies = config.proxies

        self.cancel = threading.Event()
        self.finished = threading.Event()

        #-- template
        self.session = Session(server)
        self.session.auth = (config.username, config.password)
        self.session.headers.update({'ClientVersion': clientversion})

        self.logerror = self.__logging_d(logging.error)
        self.logwarning = self.__logging_d(logging.warning)
        self.loginfo = self.__logging_d(logging.info)
        self.logdebug = self.__logging_d(logging.debug)

        #-- restore queue from file
        try:
            self.restore_queue()
        except IOError: pass
#}}}
#}}}

    def close(self):
        """Clean up the thread"""
        self.cancel.set()
        self.finished.wait(5)

    def run(self):
        """Thread loop"""
        count = counter() #{{{

        ql_old = 0
        while not self.cancel.is_set():

            #-- transfer records from the input to the output queue
            while not self.qin.empty():
                self.qout.put(self.qin.get())
            #-- wait for new record if output queue is empty
            if self.qout.empty():
                try:
                    self.qout.put(self.qin.get(timeout=1))
                except:
                    continue

            #-- pop record from the stack
            record = self.qout.get(False)
            #self.irecord += 1  #-- where does the packet counter need to increase now?

            #-- log queue size changes
            ql = self.qout.qsize()
            if ql != ql_old:
                self.logwarning("queue length: %d", ql)
                ql_old = ql

            #-- submit
            try:
                self.__submit(record)

            #-- retry on timeouts
            except requests.exceptions.Timeout as e:
                self.qout.put(record) #-- put back
                #-- don't log retries
                n = count(1) #-- error state
                if n == 0: self.logerror("Timeout: %s", e)

                self.cancel.wait(TSLEEP(n)) #-- rate limiter

            #-- retry on connection errors
            except requests.exceptions.ConnectionError as e:
                self.qout.put(record) #-- put back
                #-- don't log retries
                n = count(2) #-- error state
                if n == 0: self.logerror("%s: %s", type(e).__name__, e)

                #-- reenable proxies
                if isinstance(e, requests.exceptions.SSLError)  and \
                  self.proxies is not config.proxies:
                    n = count(20) #-- clear error state
                    self.proxies = config.proxies
                    self.logerror("reenabling proxies...")
                #-- disable proxies
                elif isinstance(e, requests.exceptions.ProxyError)  and \
                  self.proxies is not None:
                    n = count(20) #-- clear error state
                    self.proxies = None
                    self.logerror("disabling proxies...")

                self.cancel.wait(TSLEEP(n)) #-- rate limiter

            #-- don't retry on other exceptions
            except Exception as e:
                count(3) #-- error state
                self.logerror("%s: %s", type(e).__name__, e)

            #-- success
            else:
                n = count(0)  #-- success state
                if n == 0:
                    self.loginfo("submission successful")
                else:
                    self.logdebug("submission successful")
                self.cancel.wait(.1) #-- rate limiter

        #-- clean up on exit
        else:
            self.dump_queue()
            self.finished.set()
#}}}

    def __submit(self, record):
        """Submit one record to the server and handle payload errors"""
        #{{{
        r = self.session.post('/raw/' + config.facility, proxies=self.proxies, json=record, timeout=4)

        #-- raise exception if request failed
        r.raise_for_status()

        #-- successful post
        response = r.json()

        #-- capture error code
        if "error" in response:
            msg = str(response["error"]["message"])
            #-- server does not accept payload, attempt fix
            if "input does not match schema" in msg:
                record = self.__drop_violating_params(record, msg)
                if record is not None:
                    self.logwarning('dropping offending content and try again')
                    return self.__submit(record)
            raise self.SubmissionError(msg)
#}}}

    def __drop_violating_params(self, record, errmsg):
        """Extract violating parameters from errmsg and drop them from record"""
        #-- find all matches{{{
        keys, vals = [], []
        i, j = float('nan'), 0
        for m in re.finditer(self.SERVER_SCHEMA_ERROR_REGEX , errmsg):
            k = m.group(1)
            keys.append(k)
            #-- the reason for rejection is written between the matches
            j = m.start()
            if j > i: vals.append(errmsg[i:j])
            i = m.end()

        if i > 0:
            vals.append(errmsg[i:-1].rstrip(']'))

        try:
            rej_reasons = {}
            for k, v in zip(keys, vals):
                rej_reasons[k] = v
        except Exception as e:
            self.logerror('server errormsg parsing error: %d, %d; %s', len(keys), len(vals), e)
            return

        self.logwarning('payload rejected by server, reason: \'%s\'', str(rej_reasons))

        data = record["data"]
        dirty = False
        for k in rej_reasons:
            res = data.pop(k, None)
            if res is None: continue
            dirty = True

        #-- nothing was fixed
        if not dirty:
            self.logwarning('payload fix failed!')
            return

        return record
#}}}

    def __logging_d(self, func):
        """Prepend iserver and irecord on each line"""
        def wrapper(arg, *args, **kwargs):#{{{
            arg = "S%d:R%d:" + str(arg)
            return func(arg, self.iserver+1, self.irecord, *args, **kwargs)
        return wrapper
#}}}

    def restore_queue(self):
        '''Restore queue from disk, oldest first'''
        with open(self.fname, "r") as f:#{{{
            lines = f.readlines()
            for line in lines[::-1]:
                record = json.loads(line)
                self.qout.put(record)
        if len(lines) > 0:
            self.loginfo('restored queue from disk: %d records', len(lines))
#}}}

    def dump_queue(self):
        '''Save queue to disk, most recent first'''
        ql = self.qout.qsize()#{{{
        with open(self.fname, "w") as f:
            while not self.qout.empty():
                record = self.qout.get()
                json.dump(record, f)
                f.write("\n")
        if ql > 0:
            self.loginfo('saved queue to disk: %d records', ql)
#}}}
#}}}

# vim: foldmethod=marker
