# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import os
import logging

class ParameterMap:
    COLNAMES = ['parname', 'parname_site']

    class BadParameterMap(Exception): pass

    def __init__(self, fname):
        self.fname = fname

    def read(self):
        """Read the parameter.map"""
#-- only read again if file has been modified #{{{
        mtime = os.stat(self.fname).st_mtime
        try:
            if mtime == self.mtime: return
        except AttributeError:
            pass
        self.mtime = mtime

        logging.info("reading: " + self.fname)
        with open(self.fname, 'r') as f:
            lines = f.readlines()

        newparams = {}
        errors = []
        for iline, line in enumerate(lines):
            line = line.strip()
            #-- skip comment and empty lines
            if len(line) == 0: continue
            if line[0] == "#": continue

            fields = line.split()
            par = fields[0]
#-- verify lines
            #-- incomplete line
            if len(fields) < len(self.COLNAMES):
                msg = "parameter.map(%d): too few fields"
                errors.append(msg % (iline+1))
                continue
            #-- line is too long
            if len(fields) > len(self.COLNAMES):
                msg = "parameter.map(%d): too many fields"
                errors.append(msg % (iline+1))
                continue
            #-- missing parname
            if len(par) == 0:
                msg = "parameter.map(%d): missing parname"
                errors.append(msg % (iline+1))
                continue
            #-- duplicate parname
            if par in newparams:
                msg = "parameter.map(%d): duplicate parameter"
                errors.append(msg % (iline+1))
                continue

#-- put fields in a dict
            newparams[par] = fields[1]

        #-- die if errors encountered
        if len(errors) != 0:
            msg = "\n".join(errors)
            raise self.BadParameterMap(msg)

#-- save if no errors
        logging.warning("parameter.map read successfully")
        return newparams
#}}}

# vim: foldmethod=marker
