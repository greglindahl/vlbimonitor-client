# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import collections
import logging
#-- custom libraries
from . import masterlist
from . import parametermap
from . import config #-- vlbimon configuration file

class ParameterDef:
    """Parameter definition is a merge of the Masterlist and ParameterMap classes"""
    class IncompatibleParameterDefs(Exception): pass
    class IncompleteParameterDefs(Exception): pass

    def __init__(self):
        self.masterlist = masterlist.Masterlist(config.rootdir + '/masterlist.csv')
        self.parametermap = parametermap.ParameterMap(config.rootdir + '/parameter.map')

        self.parlist = collections.OrderedDict()
        self.site_parlist = {}

        self.master = self.masterlist.read()
        self.parmap = self.parametermap.read()
        self.__merge(self.master, self.parmap)


    def __merge(self, master, parmap):
        """Combine info from masterlist and parameter.map"""
        #-- sanity check{{{
        #-- all known parameter names
        known_names = []
        for k,v in master.items():
            #-- current name
            known_names.append(k)
            #-- previous names
            if 'oldnames' in v:
                known_names += v['oldnames']
        #-- check for incompatibilities
        errors = []
        for k in parmap:
            if k not in known_names:
                msg = "parameter.map: parameter '%s' not in masterlist"
                errors.append(msg % k)
                continue
        #-- die if incompatible parameters encountered
        if len(errors) != 0:
            msg = "\n".join(errors)
            raise self.IncompatibleParameterDefs(msg)

        #-- only include parameters mapped to the site
        parlist = collections.OrderedDict()
        for k,v in master.items():
            kk = k
            if not k in parmap:
                if not 'oldnames' in v: continue

                #-- does parameter.map refer to an oldname?
                oldnames = v['oldnames']
                for kk in oldnames:
                    #-- match
                    if kk in parmap:
                        break
                else:
                    continue

            if kk != k:
                logging.warning("parameter.map: parameter name '%s' is deprecated, use '%s' instead" % (kk, k))

            parlist[k] = v
            parlist[k]["parname_site"] = parmap[kk]

        #-- verify that existing couplings do not break in new version
        missing = list(set(self.parlist.keys()) - set(parlist.keys()))
        if len(missing) > 0:
            msg = "Existing couplings missing in new parameter.map: %s" % str(missing)
            raise self.IncompleteParameterDefs(msg)

        #-- adopt
        self.parlist.clear()
        self.parlist.update(parlist)

        #-- also index by parname_site
        self.site_parlist = {}
        for v in self.parlist.values():
            self.site_parlist[v["parname_site"]] = v

        logging.warning("masterlist and parameter.map merged successfully, adopted")
#}}}

    def readNew(self):
        """Re-read masterlist and parameter.map and attempt merge"""
        try:#{{{
            master = self.masterlist.read()
        except self.masterlist.BadMasterlist as e:
            logging.error(e)
            logging.error("Not using new masterlist")
            return

        try:
            parmap = self.parametermap.read()
        except self.parametermap.BadParameterMap as e:
            logging.error(e)
            logging.error("Not using new parameter.map")
            return

        try:
            if master is None  and  parmap is None:
                return
            elif master is None:
                msg = "new parameter.map"
                self.__merge(self.master, parmap)
                #-- save on success
                self.parmap = parmap
            elif parmap is None:
                msg = "new masterlist"
                self.__merge(master, self.parmap)
                #-- save on success
                self.master = master
            else:
                msg = "new masterlist and parameter.map"
                self.__merge(master, parmap)
                #-- save on success
                self.master = master
                self.parmap = parmap
        except Exception as e:
            logging.error(e)
            logging.error("Not using " + msg)
            return

        return 0
#}}}

    def warnMissingSiteParameters(self):
        """Warn about parameters that are not mapped to the site"""
        missing = set(self.master.keys()) - set(self.parlist.keys()) #{{{

        errors = []
        for k,v in self.master.items():
            if k not in missing: continue
            if 'parname_site' not in v  and  v['partype'] == "mandatory":
                msg = "%s does not yet send: %s" % (config.facility, k)
                errors.append(msg)

        if len(errors) > 0:
            logging.info("\n\t".join(errors))
#}}}

# vim: foldmethod=marker
