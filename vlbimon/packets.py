# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

#-- standard libraries
import datetime
import logging
try:
    import queue
except ImportError:
    import Queue as queue
import subprocess
#-- 3rd-party libraries
import json
#-- custom libraries
from . import config  #-- vlbimon configuration file

class Records(object):
    def __init__(self):
        #-- settings
        username = config.username.replace(' ', '_')
        self.fname_irecord = config.storagedir + "/client_ipacket.{:}.{:}".format(config.facility, username)
        self.fname_storage = config.storagedir + "/{:}.{:}".format(config.facility, username) + ".{:}" # date

        #-- read record counter from file
        self.ipacket = self.__read_irecord()

        #-- one fifo queue per server
        self.queues = []
        for server in config.servers:
            q = queue.Queue() #-- Fifo
            q.server = server
            self.queues.append(q)


    def put(self, data):
        #-- insert record in all queues
        for q in self.queues:
            q.put(data)

        #-- debug output
        logging.debug('record queued: %s', json.dumps(data))

        #-- save counter and record to disk
        self.__write_irecord()
        self.__write_record(data)


    def __read_irecord(self):
        '''read record counter'''
        try:
                with open(self.fname_irecord, "r") as f:
                        ipacket = int(f.read())
        except IOError:
                ipacket = 0
        return ipacket


    def __write_irecord(self):
        '''save record counter'''
        with open(self.fname_irecord, "w") as f:
            f.write("{:}".format(self.ipacket))


    def __write_record(self, record):
        '''save record to disk'''
        now = datetime.datetime.utcnow()
        fname = self.fname_storage.format(now.strftime("%Y%m%d"))
        with open(fname, "a") as f:
            json.dump(record, f)
            f.write("\n")
