Update Instructions
===================
1. Before updating, stop a running client:

		# systemctl stop vlbimon-client
		# pgrep client.py

2. Update to latest upstream:

		# cd vlbimonitor-client
		# git status -v
		# git pull

3. Adopt changes from config-sample.py in your config.py:

		# vimdiff vlbimon/config-sample.py vlbimon/config.py


From v2 to v3:
--------------
1. In your parameter.map file, convert formal parameter names to the "_" delimiter, backing up the original file to parameter.map.bak:

		# perl -pi.bak -e '1 while s/^([^#:\t]*):/\1_/' parameter.map

2. In the vlbimon/config.py file, drop the /rpc endpoint name from the servers definition:
		# perl -pi.bak -e 's+(vlbimon.\.science\.ru\.nl)/rpc+\1' vlbimon/config.py


From v1 to v2:
--------------
The sitedata API stays the same.

Client module configuration and input files change as follows (use ALMA as example):

1. Move packet counter file

		# mv 'client_ipacket.ALMA.ALMA central' local_storage/client_ipacket.ALMA.ALMA_central

2. Adopt vlbimon config settings

		# cp vlbimon/config-sample.py  vlbimon/config.py
		# vimdiff vlbimonconf.py vlbimon/config.py
		# rm vlbimonconf.py vlbimonconf.pyc

3. Move sitedata.py to new location

		# ls -l sitedata.py   #-- sitedata.py -> sitedata-ALMA.py
		# mv sitedata-ALMA.py sitedata/ALMA.py
		# rm sitedata.py sitedata.pyc
		# echo 'from ALMA import *' > sitedata/__init__.py

4. Create parameter.map file from masterlist-ALMA.csv

		# tools/gen-parameter.map masterlist-ALMA.csv > sitedata/ALMA.map
		# ln -sf sitedata/ALMA.map parameter.map


From v0 to v1:
--------------
The sitedata API changes:

1. Values stored in the sitedata.params dict change from a single [x,y] datapoint to an array of [x,y] data points per key.  Update the sitedata module by replacing [x,y] -> [[x,y]]

2. The sitedata.updateValues function is renamed to sitedata.update_values

3. Adopt any new vlbimon config settings

		# vimdiff vlbimonconf.py vlbimonconf-sample.py
