# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

class Sitedata(object):
    """Implements site-dependent data acquisition."""
    def __init__(self, sitelist):
        self.sitelist = sitelist

    def close(self):
        """Clean up this class, e.g. close any threads used"""
        pass

    def collect(self):
        """Collect data points"""
        #-- start with empty dict
        params = {}

        #-- Insert [t, value] data points, where t is the time at
        #-- which the value was originally measured.
        #-- We use dummy measurements here performed at a dummy time.
        #-- Don't use the 'time' module in production!
        import time
        t = time.time()  #-- don't use this in production
        try:
            self.onsource = not self.onsource
            self.tsys1 += 1
            self.temp += 1
        except:
            self.onsource = False
            self.tsys1 = 35
            self.temp = 315
        #-- this parameter has a 0 cadence defined in masterlist.csv
        #-- every change will be submitted
        params['telescope.onSource'] = [[t, self.onsource]]
        #-- this parameter has a non-zero cadence defined in masterlist
        #-- changes will be submitted at a limited rate
        params['if1.tSys'] = [[t, self.tsys1]]
        params['weather.temperature'] = [[t, self.temp]]

        return params
