# This file is part of the VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved
from __future__ import print_function

import sys
import logging
import mark6mon

try:
    #-- use a non-standard set of recorders
    from vlbi_config import *
except ImportError:
    #-- save a departing definition of RECORDER_HOSTNAMES in a vlbi_config.py file
    RECORDER_HOSTNAMES = ["recorder1", "recorder2", "recorder3", "recorder4"]

class Sitedata():
    """Implements site-dependent data acquisition."""
    def __init__(self, sitelist):#{{{
        self.sitelist = sitelist

        #-- instantiate recorder objects
        self.recorders = []
        for i, hostname in enumerate(RECORDER_HOSTNAMES):
            recorder = mark6mon.Recorder(hostname)
            recorder.parname_prefix = "recorder_{:}_".format(i+1) #-- each recorder has uniq parameter names
            self.recorders.append(recorder)

    def close(self, signum, frame):
        """Close all threads and subprocesses."""
        #-- close all units #{{{
        for i,recorder in enumerate(self.recorders):
            logging.info('Sitedata: closing recorder %d', i+1)
            recorder.close()

        #-- log
        logging.error('ABORT: signal %d detected', signum)
        #raise Exception('signal %d detected' % signum)
#}}}

    def collect(self):
        """Collect data points from all sources"""
        #-- start with empty dict{{{
        params = {}

        #-- insert values from each recorder
        for i, recorder in enumerate(self.recorders):
            #-- read the logs
            params_ = recorder.collect()

            #-- unique parameter names
            params_ = {recorder.parname_prefix + k: v for k,v in params_.items()}
            params.update(params_)
        return params
#}}}

class Getter(Sitedata):
    """Compatibility interface to Sitedata"""
    def __init__(self, sitelist):
        Sitedata.__init__(self, sitelist)

    def update_values(self):
        self.params = self.collect()
#}}}

# vim: foldmethod=marker
