observatory:timeZone	OBS.TIMEZONE

observerMessages:observer	MESS.MESSAGE
observerMessages:observatoryStatus	MESS.STATUS
observerMessages:weather	MESS.WEATHER

telescope:status	TEL.STATUS
telescope:observingMode	TEL.OBS_MODE
telescope:onSource	TEL.ON_SOURCE
telescope:apparentRaDec	TEL.APP_RA_DEC
telescope:sourceName	TEL.SOURCE_NAME
telescope:epochRaDec	TEL.EPOCH_RA_DEC
telescope:epochType	TEL.EPOCH
telescope:azimuthElevation	TEL.AZEL

gainElevation:status	GAIN.STATUS

phasing:phasing	PHASING.PHASING_NOW
phasing:referenceAntennaID	PHASING.REF_ANT
phasing:referencePad	PHASING.REF_PAD
phasing:referenceCoordinates	PHASING.REF_COORDS
phasing:referenceAltitude	PHASING.REF_ALT
phasing:phaseCenter	PHASING.PHASE_CENTER
phasing:antennaIDs	PHASING.IDS
phasing:numberOfAntennas	PHASING.N_ANTENNAS
phasing:status	PHASING.STATUS
phasing:efficiency	PHASING.EFF

weather:online	ENVIRO.ONLINE
weather:temperature	ENVIRO.Air_Temp
weather:airPressure	ENVIRO.Pressure
weather:relativeHumidity	ENVIRO.Humidity
weather:windSpeed	ENVIRO.Wind_Speed
weather:windDirection	ENVIRO.Wind_Dir
weather:tau225	ENVIRO.SMA_TAU
weather:wvrStatus	ENVIRO.WVR_PRESENT
weather:pwv	ENVIRO.PWV
weather:atmosphericPhaseRMS	ENVIRO.PM
weather:precipCondition	ENVIRO.rain_event

rx:1:online	RXA_ENG_TASK_ONLINE
rx:1:loFreq	RXA_ENG_TASK.LO_FREQ_DEMAND
rx:1:lockStatus	RXA_ENG_TASK.ALL_LOCKED
rx:1:qwpInserted	RXA_ENG_TASK.WAVEPLATE_IN
rx:1:nrIFs	RXA_ENG_TASK.NRIFS
rx:1:calTemp	RXA_ENG_TASK.CAL_TEMP
rx:2:online	RXB_ENG_TASK_ONLINE
rx:2:loFreq	RXB_ENG_TASK.LO_FREQ_DEMAND
rx:2:lockStatus	RXB_ENG_TASK.ALL_LOCKED
rx:2:qwpInserted	RXB_ENG_TASK.WAVEPLATE_IN
rx:2:nrIFs	RXB_ENG_TASK.NRIFS
rx:2:calTemp	RXB_ENG_TASK.CAL_TEMP

if:1:online	IF1.ONLINE
if:1:centreFreq	IF1.CENTER_FREQ
if:1:bandWidth	IF1.BANDWIDTH
if:1:systemTemp	IF1.TSYS
if:1:systemTempAzel	IF1.TSYS_AZEL

if:2:online	IF2.ONLINE
if:2:centreFreq	IF2.CENTER_FREQ
if:2:bandWidth	IF2.BANDWIDTH
if:2:systemTemp	IF2.TSYS
if:2:systemTempAzel	IF2.TSYS_AZEL

maser:batteryVoltageA	MASER.battery_voltage_a
maser:batteryCurrentA	MASER.battery_current_a
maser:batteryVoltageB	MASER.battery_voltage_b
maser:batteryCurrentB	MASER.battery_current_b
maser:hydrogenPressureSetting	MASER.hydrogen_pressure_setting
maser:hydrogenPressureMeasurement	MASER.hydrogen_pressure_measurement
maser:purifierCurrent	MASER.purifier_current
maser:dissociatorCurrent	MASER.dissociator_current
maser:dissociatorLight	MASER.dissociator_light
maser:heaterInternalTop	MASER.heater_internal_top
maser:heaterInternalBottom	MASER.heater_internal_bottom
maser:heaterInternalSide	MASER.heater_internal_side
maser:heaterThermalControlUnit	MASER.heater_thermal_control_unit
maser:heaterExternalSide	MASER.heater_external_side
maser:heaterExternalBottom	MASER.heater_external_bottom
maser:heaterIsolator	MASER.heater_isolator
maser:heaterTube	MASER.heater_tube
maser:boxesTemperature	MASER.boxes_temperature
maser:boxesCurrent	MASER.boxes_current
maser:ambientTemperature	MASER.ambient_temperature
maser:cfieldVoltage	MASER.cfield_voltage
maser:varactorVoltage	MASER.varactor_voltage
maser:externalHighVoltageValue	MASER.external_high_voltage_value
maser:externalHighVoltageCurrent	MASER.external_high_voltage_current
maser:internalHighVoltageValue	MASER.internal_high_voltage_value
maser:internalHighVoltageCurrent	MASER.internal_high_voltage_current
maser:hydrogenStoragePressure	MASER.hydrogen_storage_pressure
maser:hydrogenStorageHeater	MASER.hydrogen_storage_heater
maser:piraniHeater	MASER.pirani_heater
maser:405kHzAmplitude	MASER.405kHz_amplitude
maser:ocxoVoltage	MASER.ocxo_voltage
maser:supplyP24V	MASER.supply_p24V_voltage
maser:supplyP15V	MASER.supply_p15V_voltage
maser:supplyM15V	MASER.supply_m15V_voltage
maser:supplyP5V	MASER.supply_p5V_voltage
maser:supplyM5V	MASER.supply_m5V_voltage
maser:supplyP8V	MASER.supply_p8V_voltage
maser:supplyP18V	MASER.supply_p18V_voltage
maser:lock	MASER.lock
maser:dds	MASER.dds

bdc:1:dcFreq	BDC1.DC_FREQ

# vim: ts=40 nowrap
