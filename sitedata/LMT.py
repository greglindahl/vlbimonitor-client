from __future__ import print_function

import time
from dreampy.onemm.netcdf import OnemmNetCDFFile

filename = '/data_lmt/vlbi1mm/vlbi1mm.nc'

sys.path.append('lmtmc@mnc:~/')

nc = OnemmNetCDFFile(filename)

head = nc.hdu.header

class Getter():
    def __init__(self, sitelist):
        self.params = {}
        self.sitelist = sitelist

    def updateValues(self):
        self.params.clear()

        #-- default timestamp: now
        #-- time: seconds since 1970 Jan 1 UTC, also known as "unix time".
        utime = time.mktime(time.localtime())

        #-- insert any new values with times at which those values were measured
        #-- if no measurement time is available, use the current time
        lat = head.get('TimePlace.ObsLongitude')*(180./pi)
        lon = head.get('TimePlace.ObsLatitude')*(180./pi)
        self.params['observatory:coordinates'] = [utime, "%.3f+%.3f/" %(lat,lon)]

        self.params['Dcs.ObsPgm'] = [utime, head.get('Dcs.ObsPgm')]

        sourceRA = (head.get('Source.Ra')[0]/(2.*pi))*24.
        sourceDec = (head.get('Source.Dec')[0]/(pi/2.))*90.
        sourceAz = (head.get('Sky.AzReq')[0])*(180./pi)
        sourceEl = (head.get('Sky.ElReq')[0])*(180./pi)

        self.params['currentPointing'] = [utime, "%.3f-%.3f" %(sourceRA, sourceDec)]
        self.params['currentPointing.azimuthElevation'] = [utime, "%.4f+%.4f" %(sourceAz, sourceEl)]

        self.params['epoch'] = [utime, head.get('Source.Epoch')]
        self.params['currentlySelectedSource'] = [utime, head.get('Source.SourceName')]

        if head.get('Dcs.ObsPgm') == 'Cal':
            Tsys = nc.hdu.process_scan()
            apow = Tsys['APower']
            bpow = Tsys['BPower']
            self.params['systemTemperature'] = [utime, "%.2f, %.2f" %(apow,bpow)]

        self.params['Weather.WindSpeed2'] = [utime, head.get('Weather.WindSpeed2')]
        self.params['Weather.Humidity'] = [utime, head.get('Weather.Humidity')]
        self.params['Weather.Pressure'] = [utime, head.get('Weather.Pressure')]
        self.params['Radiometer.Tau'] = [utime, head.get('Radiometer.Tau')]
        self.params['Weather.Temperature'] = [utime, head.get('Weather.Temperature')]
        if head.get('Weather.Precipitation'):
            self.params['Weather.Precipitation'] = [utime, "rain"]
        else:
            self.params['Weather.Precipitation'] = [utime, "dry"]
