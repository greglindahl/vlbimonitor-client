'''
JCMT-specific sitedata module for VLBI client.
Author Ryan Berthold, EAO

TODO: What kind of copyright/license boilerplate does this need?
'''
from __future__ import print_function

import time
import calendar
import threading
import signal
import ctypes
import socket
import os
import math
import drama
import drama.log
import logging
from functools import wraps

#-- limit significant figures
def signif(n, x):
    return float("%.*g" % (n, x))

taskname = 'VLBIMON'

drama.log.setup()  # no taskname = no file output
log = logging.getLogger(taskname)

# used to delay signal handler install until after drama.init
sem = threading.Semaphore(0)

# used to protect access to global param buffers
lock = threading.Lock()

# i don't trust calling drama methods outside the drama thread,
# so a socket callback is used to shut down the run() loop on sigint.
s1,s2 = socket.socketpair()

tparams = {} # internal buffer


# decorator to retry monitors as needed
# TODO put this into a drama utility module
def retry_monitor(task, param):
        def decorator(f):
            print('retry_monitor %s: %s.%s' % (f, task, param))
            @wraps(f)
            def wrapper(*args):
                #print('wrapper called, args: %s' % (str(args)))
                msg = args[-1]  # allow for 'self' if f is a method
                act = f.__name__  # TODO ask drama
                if msg.reason == drama.REA_OBEY:
                    log.info(act + ' started.')
                    drama.reschedule(0)  # to start monitor
                    return
                elif msg.reason == drama.REA_RESCHED:
                    log.info('%s trying monitor on %s.%s...' % (act, task, param))
                    try:
                        drama.monitor(task, param)
                    except:
                        log.exception('monitor exception, will retry in 5s...')
                        drama.reschedule(5)
                        return
                elif msg.reason == drama.REA_TRIGGER and msg.status == drama.MON_STARTED:
                    log.info('%s.%s monitor started.' % (task, param))
                elif msg.reason == drama.REA_TRIGGER and msg.status == drama.MON_CHANGED:
                    f(*args)
                else:
                    log.error('unhandled entry reason, will retry monitor in 5s. msg: %s', msg)
                    drama.reschedule(5)
                    return
                drama.reschedule()
                return
            return wrapper
        return decorator


@retry_monitor('ENVIRO@jcmt-aux', 'DYN_STATE')
def MON_ENVIRO(msg):
    try:
        utime = time.time()
        ttime = calendar.timegm(time.strptime(msg.arg['JCMT_TAU_TIME'], '%Y-%m-%dT%H:%M:%S'))
        # dry, rain, hail, snow, frost, dew
        precip = 'dry'
        if msg.arg['rain_event']:
            precip = 'rain'
        elif msg.arg['hail_event']:
            precip = 'hail'
        with lock:
            tparams['ENVIRO.JCMT_TAU'] = [ttime, msg.arg['JCMT_TAU']]
            tparams['ENVIRO.Air_Temp'] = [utime, signif(4, msg.arg['Air_Temp'] + 273.15)]  # C to K
            tparams['ENVIRO.Pressure'] = [utime, signif(4, msg.arg['Pressure'] * 0.1)]  # mbar to KPa
            tparams['ENVIRO.Humidity'] = [utime, signif(4, msg.arg['Humidity'] * 0.01)]  # pct to fraction
            tparams['ENVIRO.Wind_Speed'] = [utime, signif(4, msg.arg['Wind_Speed'] * 0.44704)]  # mph to m/s
            tparams['ENVIRO.rain_event'] = [utime, precip]
    except:
        log.exception('MON_ENVIRO handler exception')


@retry_monitor('RXA_ENG_TASK@rxa', 'DYN_STATE')
def MON_RXA(msg):
    try:
        utime = time.time()
        with lock:
            tparams['RXA_ENG_TASK.LO_FREQ_DEMAND'] = [utime, msg.arg['LO_FREQ_DEMAND']]
            tparams['RXA_ENG_TASK.PHASE_LOCKED'] = [utime, bool(msg.arg['PHASE_LOCKED'])]
    except:
        log.exception('MON_RXA handler exception')


def GET_TEL(msg):
    try:
        
        try:
            utime = time.time()
            onsource = drama.get('THI@mamo', 'DISPLAY').wait(1.0).arg['DISPLAY']['ONSOURCE'] >= 0.0
            tracking = drama.get('THI@mamo', 'TRACKING').wait(1.0).arg['TRACKING']
            if onsource:
                mode = 'tracking'
            else:
                if tracking:
                    mode = 'slewing'
                else:
                    mode = 'idle'
            with lock:
                tparams['TEL.OBS_MODE'] = [utime, mode]
                tparams['TEL.ON_SOURCE'] = [utime, onsource]
        except:
            #log.exception('THI exception')
            print('THI exception, skipping details.')
        
        try:
            science = drama.get('PTCS@mamo', 'SCIENCE').wait(1.0).arg['SCIENCE']
            name = science['ID']
            dj2000 = drama.get('PTCS@mamo', 'DJ2000_BASE').wait(1.0).arg['DJ2000_BASE']
            mean = '%.12f%+.12f' % (dj2000['C1_HR'], dj2000['C2_DEG'])
            epoch = 'J2000'
            rapp = drama.obey('PTCS@mamo', 'GET_BASE', SYSTEM='APP').wait(1.0).arg
            app = '%.12f%+.12f' % (rapp['C1']*12.0/math.pi, rapp['C2']*180.0/math.pi)
            with lock:
                tparams['TEL.APP_RA_DEC'] = [utime, app]
                tparams['TEL.SOURCE_NAME'] = [utime, name]
                tparams['TEL.EPOCH_RA_DEC'] = [utime, mean]
                tparams['TEL.EPOCH_TYPE'] = [utime, epoch]
        except:
            #log.exception('PTCS exception')
            print('PTCS exception, skipping details.')
        
    except:
        log.exception('GET_TEL action exception')
    drama.reschedule(4.0)


def exit_callback(fd):
    log.info('exit_callback raising Exit')
    raise drama.Exit('exit_callback')


def main():
    try:
        log.info('starting.')
        log.info('calling drama.init()')
        drama.init(taskname, buffers=[64000, 8000, 8000, 2000], actions=[MON_ENVIRO, MON_RXA, GET_TEL])
        drama.register_callback(s2, exit_callback)
        sem.release()
        log.info('starting monitors.')
        drama.blind_obey(taskname, 'MON_ENVIRO')
        drama.blind_obey(taskname, 'MON_RXA')
        drama.blind_obey(taskname, 'GET_TEL')
        log.info('calling drama.run()')
        drama.run()
    finally:
        log.info('finally: releasing sem.')
        sem.release()
        log.info('calling drama.stop()')
        drama.stop()
        log.info('sending SIGINT to interrupt main thread.')
        os.kill(0, signal.SIGINT)
        log.info('done.')

thread = threading.Thread(target=main)
#thread.daemon = True
thread.start()

# wait for drama init before re-installing signal handlers
sem.acquire()

orig_handlers = {signal.SIGINT: signal.SIG_DFL,
                 signal.SIGTERM: signal.SIG_DFL}

def handler(num, frame):
    print('signal handler: restoring original handler for signal', num)
    signal.signal(num, orig_handlers[num])
    print('signal handler: sending "exit" on s1')
    s1.send('exit')
    print('signal handler: joining thread')
    thread.join()

orig_handlers[signal.SIGINT] = signal.signal(signal.SIGINT, handler)
orig_handlers[signal.SIGTERM] = signal.signal(signal.SIGTERM, handler)

class Getter():
    def __init__(self, sitelist):
        self.params = {}
        self.sitelist = sitelist

    def updateValues(self):
        '''Called by client.py to update params dict from internal buffer.'''
        self.params.clear()
        if not thread.is_alive():
            print('updateValues: thread done, calling sys.exit.')
            sys.exit(0)
        with lock:
            self.params.update(tparams)
