# This is a VLBImonitor client.  This client is released under the terms of the MIT Licence.
# Copyright (c) 2016 Radboud RadioLab.  All rights reserved

#-- standard libraries
import datetime
import glob
import os
import queue
import socket
import struct
import threading
import time

import logging
logger = logging.getLogger(__name__)
logging.getLogger("becs").setLevel(logging.WARN)

#-- add-on libraries
import redis

#-- from vlbicontrol
import becs.bdc
import becs.config
import becs.mark6
import becs.r2dbe
import config
import r2daemon
import vdif
import vexinterpreter
import vexparser

backend_config_file = "/etc/backend.conf"

#-- helper functions
#-- limit significant figures{{{
def signif(n, x):
    """Print float to specified number of significant figures"""
    return float("%.*g" % (n, x))

def nsignif(x):
    '''Return number of significant figures'''
    return len(str(x).split('e')[0].replace(".", ""))

def ndecimal(x):
    '''Return the number of decimals in the input'''
    words = str(x).split(".")
    if len(words) < 2: return 0
    return len(words[1])
#}}}

class Sitedata(object):
    """Implements site-dependent data acquisition."""
    def __init__(self, sitelist):
        self.sitelist = sitelist
        #-- initialise and run dplane status message receiver
        self._dpsm_receiver = DPlaneStatusMessageReceiver()
        self._dpsm_receiver.start()
        #-- refresh config first time, forcing update
        self._refresh_config(force=True)
        #-- refresh schedules
        self._known_scheds = {}
        self._refresh_sched()

    def _refresh_config(self, force=False):
        config_mtime = os.path.getmtime(backend_config_file)
        if force or self.config_mtime < config_mtime:
            logger.debug("refreshing config")
            #-- read config
            self.config = config.Config(backend_config_file)
            #-- clear dplane status message receiver mappings
            self._dpsm_receiver.clr_dpsm_queue_mapping()
            #-- create sub-acquirer for each device
            self.acquirers = []
            for devname in becs.config.get_all_device_names(self.config):
                devdict = self.config.devices[devname]
                cls = None
                for layout, mapped_cls in [
                  ("r2dbe", R2DBEData),
                  ("bdc_1band", SingleBandBDCData),
                  ("bdc_2band", DualBandBDCData),
                  ("mark6", Mark6Data),
                ]:
                    if devdict["layout"] == layout:
                        cls = mapped_cls
                if cls:
                    logger.debug("adding acquirer %s(%s)", cls.__name__, devname)
                    obj = cls(devname, self.config)
                    self.acquirers.append(obj)
                    # for Mark6Data, add mapping to dplane status message receiver
                    if isinstance(obj, Mark6Data):
                        self._dpsm_receiver.add_dpsm_queue_mapping(*obj.get_addr_queue())
            #-- update last known modify-time
            self.config_mtime = config_mtime

    def _refresh_sched(self):
        #-- find all valid schedules from trigger and testing area
        stationcode = self.config.instruct["stationcode"]
        valids = []
        for area in ["trigger", "testing"]:
            # look for schedules in the designated area
            candidates = glob.glob(os.sep.join([self.config.instruct["vexstore"], area, "*.vex"]))
            if not candidates:
                continue
            # filter out invalid schedules
            now = datetime.datetime.utcnow()
            for candidate in candidates:
                vex_dict = vexparser.vexparser(candidate)
                vex = vexinterpreter.Vex(vex_dict)
                vex.schedule.set_station(stationcode)
                # any scans?
                if not vex.schedule.scans:
                    continue
                # scans with future end date? (only check last scan)
                if vex.schedule.scans[-1].stop <= now:
                    continue
                valids.append(vex)
        #-- update known schedules
        for md5sum in list(self._known_scheds.keys()):
            # first remove known schedules that are not among valids
            if md5sum not in [v.md5sum for v in valids]:
                logger.debug("found known schedule no longer valid, removing (md5sum '%s')", md5sum[:7])
                # remove entry from mapping
                srr = self._known_scheds.pop(md5sum)
                # stop the schedule redis reporter thread
                srr.set_stopped()
                srr.join()
        for v in valids:
            # add unknown schedules among valids
            if v.md5sum not in list(self._known_scheds.keys()):
                logger.debug("found valid schedule which is unknown, adding (md5sum '%s')", v.md5sum[:7])
                # create schedule redis reporter thread and start
                srr = ScheduleRedisReporter(v)
                srr.start()
                # add entry to mapping
                self._known_scheds[v.md5sum] = srr

    def close(self, signum, frame):
        """Clean up this class, e.g. close any threads used"""
        logger.debug("Sitedata received signal %d, closing...", signum)
        # close any schedule redis reporters
        logger.debug("stopping schedule redis reporter threads")
        [srr.set_stopped() for srr in list(self._known_scheds.values())]
        [srr.join() for srr in list(self._known_scheds.values())]
        # close dplane status message receiver
        logger.debug("stopping dplane status message receiver thread")
        self._dpsm_receiver.set_stopped()
        self._dpsm_receiver.join()

    def collect(self):
        """Collect data points"""
        #-- start with empty dict
        params = {}

        #-- generate self-parameters

        #-- refresh config
        self._refresh_config()

        #-- refresh schedule
        self._refresh_sched()

        #-- collect from acquirers in separate threads
        acq_thds = [threading.Thread(target=acq.collect_and_update, args=(params,)) for acq in self.acquirers]
        [at.start() for at in acq_thds]
        [at.join() for at in acq_thds]

        return params

class Getter(Sitedata):
    """Forwards compatibility interface to Sitedata.

    Getter is deprecated, use Sitedata instead."""
    def __init__(self, sitelist):#{{{
        super(Getter, self).__init__(sitelist)
        self.params = {}
#}}}

class R2DBEData(object):
    """Implements R2DBE-specific data acquisition."""
    def __init__(self, name, config):#{{{
        """Instantiate an R2DBE-data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.name = name
        self.device = becs.config.get_r2dbe_inst(name, config, becs.r2dbe.R2DBE)
        self.redis = redis.Redis()
        self.last_update = -1.0

        logging.getLogger("becs.r2dbe").setLevel(logging.WARN)

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()
        #-- only report status and stop for unreachable device
        if not self.device.test_device_reachable():
            export_params.update(self._prefix_params({"status": [[now, "unreachable"]]}))
            return
        #-- report status as (un)configured
        params["status"] = [[now, "configured" if self.device.is_configured() else "unconfigured"]]

        #-- check if new update since last
        last_update = float(self.redis[self._redis_key("_last_update")])
        if not last_update > self.last_update:
            export_params.update(params)

        #-- parameters with 1-to-1 mapping to _get_param
        # split into filtered and non-filtered parameters?
        for p in [
          *["pps_offset", "uptime"],
          *[x % ch for ch in range(2) for x in [
            "pol_%d","rx_sb_%d","bdc_ch_%d","station_%d","2bit_th_%d"
          ]],
        ]:
            params[p] = [[last_update, self._get_param(p)]]

        #-- serial number
        params["serialNumber"] = self.device.serial

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.device.host,k))] = params_in[k]
        return params_out

    def _redis_key(self, key):
        return bytes(ord(c) for c in (self.name + ".raw." + key))

    def _get_param(self, param):
        param_dict = becs.r2dbe.data.lookup_param(param)
        dev = param_dict["dev"] if "snap" not in list(param_dict.keys()) else param_dict["snap"]["data"]
        redis_key = self._redis_key(dev)
        try:
            binary = r2daemon.decode(self.redis[redis_key])
        except KeyError:
            logger.warn("could not read parameter '%s' for %s (redis KeyError '%s')", param, self.name, redis_key)
            return
        return becs.r2dbe.data.decode_param(param, binary)
#}}}

class SingleBandBDCData(object):
    """Implements BDC-specific data acquisition."""
    def __init__(self, name, config):#{{{
        """Instantiate an Single-band BDC data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.name = name
        self.device = becs.config.get_bdc_1band_inst(name, config, becs.bdc.SingleBandBDC)

        logging.getLogger("becs.bdc").setLevel(logging.WARN)

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()
        #-- only report status and stop for unreachable device
        if not self.device.test_device_reachable():
            export_params.update(self._prefix_params({"status": [[now, "unreachable"]]}))
            return
        #-- report status as (un)configured
        params["status"] = [[now, "configured" if self.device.is_configured() else "unconfigured"]]

        #-- get idn parameters
        idn = self.device.get_id()
        for p in ["serial number", "hardware version", "firmware version"]:
            params[p.replace(" ", "_")] = [[now, idn[p]]]

        #-- configure and control status
        map_conf = {"DUAL":"Dual-band", "SINGLE":"Single-band"}
        params["configuration"] = [[now, map_conf[self.device.get_conf()]]]
        map_ctrl = {"NONE":"None", "LOCAL":"Front-panel only", "REMOTE":"Remote only", "BOTH":"Front-panel/Remote", "LOCK_TMP":"Temporary lock-out (front-panel input in progress)"}
        params["control_status"] = [[now, map_ctrl[self.device.get_ctrl_status()]]]

        #-- LO frequency and lock status
        lock = self.device.get_lock_status()
        for k,v in list(lock.items()):
            params["lo_status_%s" % k] = [[now, v]]
        freq = self.device.get_lo_freq()
        for k,v in list(freq.items()):
            params["lo_freq_%s" % k] = [[now, v]]

        #-- Attenuator values
        attn = self.device.get_attn()
        for k,v in list(attn.items()):
            params["attn_%s" % k] = [[now, v]]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.device.host,k))] = params_in[k]
        return params_out
#}}}

class DualBandBDCData(object):
    """Implements BDC-specific data acquisition."""
    def __init__(self, name, config):#{{{
        """Instantiate an Dual-band BDC data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.name = name
        self.device = becs.config.get_bdc_2band_inst(name, config, becs.bdc.DualBandBDC)

        logging.getLogger("becs.bdc").setLevel(logging.WARN)

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        params = {}

        now = time.time()
        #-- only report status and stop for unreachable device
        if not self.device.test_device_reachable():
            export_params.update(self._prefix_params({"status": [[now, "unreachable"]]}))
            return
        #-- report status as (un)configured
        params["status"] = [[now, "configured" if self.device.is_configured() else "unconfigured"]]

        #-- get idn parameters
        idn = self.device.get_id()
        for p in ["serial number", "hardware version", "firmware version"]:
            params[p.replace(" ", "_")] = [[now, idn[p]]]

        #-- configure and control status
        map_conf = {"DUAL":"Dual-band", "SINGLE":"Single-band"}
        params["configuration"] = [[now, map_conf[self.device.get_conf()]]]
        map_ctrl = {"NONE":"None", "LOCAL":"Front-panel only", "REMOTE":"Remote only", "BOTH":"Front-panel/Remote", "LOCK_TMP":"Temporary lock-out (front-panel input in progress)"}
        params["control_status"] = [[now, map_ctrl[self.device.get_ctrl_status()]]]

        #-- Currently selected band
        params["band"] = [[now, self.device.get_band()]]

        #-- LO frequency and lock status
        lock = self.device.get_lock_status()
        for k,v in list(lock.items()):
            params["lo_status_%s" % k.replace("-","to")] = [[now, v]]
        freq = self.device.get_lo_freq()
        for k,v in list(freq.items()):
            params["lo_freq_%s" % k.replace("-","to")] = [[now, v]]

        #-- Attenuator values
        attn = self.device.get_attn()
        for k,v in list(attn.items()):
            params["attn_%s" % k] = [[now, v]]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.device.host,k))] = params_in[k]
        return params_out
#}}}

def scan_in_progress():
    """Returns True if active scan in progress

    The test for an active scan checks whether any schedule in the redis
    database has its state set to 'scan'."""
    in_scan = False
    r = redis.Redis()
    for k in r.keys(b"schedule.*.state"):
        if r[k] == b"scan":
            in_scan = True
    return in_scan

def gap_in_progress():
    """Returns True if between scans

    The test for a gap checks whether any schedule in the redis database
    has its state set to 'gap'."""
    in_gap = False
    r = redis.Redis()
    for k in r.keys(b"schedule.*.state"):
        if r[k] == b"gap":
            in_gap = True
    return in_gap

class ScheduleRedisReporter(threading.Thread):
    """Publishes schedule information to redis"""
    def __init__(self, vex, *args, **kwargs):#{{{
        """Instantiate a schedule reporter thread"""
        super(ScheduleRedisReporter, self).__init__(*args, name=vex.md5sum[:self.MD5SUM_SHORT], **kwargs)
        self.redis = redis.Redis()
        self.vex = vex
        #-- set prefix used for redis entries
        vex_area, vex_basename = self.vex.filename.split(os.sep)[-2:]
        self.vex_area = vex_area
        vex_name = vex_basename.split(os.extsep)[0]
        self.vex_name = vex_name
        self.prefix = ".".join(["schedule", vex_area, vex.md5sum[:self.MD5SUM_SHORT]])
        #-- event used to stop thread
        self.stopped = threading.Event()
        #-- number of seconds before scan-start / after scan-stop when GAP
        self.margin = 3

    def run(self):

        #-- initialise
        now = datetime.datetime.utcnow()
        ttl = int((self.vex.schedule.scans[-1].stop - now).total_seconds() + self.margin)
        self.redis.set(self.prefix, self.vex_name, ex=ttl)
        self.redis.set(".".join([self.prefix, "start"]), self.vex.schedule.scans[0].start.strftime("%Yy%jd-%Hh%Mm%Ss"), ex=ttl)
        self.redis.set(".".join([self.prefix, "end"]), self.vex.schedule.scans[-1].stop.strftime("%Yy%jd-%Hh%Mm%Ss"), ex=ttl)

        while True:

            now = datetime.datetime.utcnow()

            #-- exit if stopped
            if self.is_stopped:
                break

            #-- update schedule state
            wait = 1
            n_scans_left = len(self.vex.schedule.scans)
            for n, scan in enumerate(self.vex.schedule.scans):
                #-- skip scans that have passed
                if scan.stop + datetime.timedelta(seconds=self.margin) <= now:
                    logger.debug("skipping scan (stopped at %s)", scan.stop)
                    n_scans_left -= 1
                    continue
                #-- this is the first scan with future stop-time
                if scan.start - datetime.timedelta(seconds=self.margin) > now:
                    #-- wait: first scan-start is more than margin in the future
                    #-- gap: non-first scan-start is more than margin in the future
                    state = "gap" if n > 0 else "wait"
                    ttl_ms = int((scan.start - now).total_seconds()*1000)
                    logger.debug("state=%s, scan.start=%s, now=%s, ttl_ms=%d", state, scan.start.strftime("%H:%M:%S"), now.strftime("%H:%M:%S"), ttl_ms)
                    wait = ttl_ms/1000 - self.margin
                else:
                    #-- scan: scan-start is at most margin in the future
                    state = "scan"
                    ttl_ms = int(((scan.stop - now).total_seconds() + self.margin)*1000)
                    logger.debug("state=%s, scan.stop=%s, now=%s, ttl_ms=%d", state, scan.stop.strftime("%H:%M:%S"), now.strftime("%H:%M:%S"), ttl_ms)
                    wait = ttl_ms/1000
                #-- set state with correct TTL, only when not already set
                self.redis.set(".".join([self.prefix, "state"]), state, px=ttl_ms)
                #-- add scan start & stop times
                self.redis.set(".".join([self.prefix, "scan", "start"]), scan.start.strftime("%Yy%jd-%Hh%Mm%Ss"),px=ttl_ms)
                self.redis.set(".".join([self.prefix, "scan", "stop"]), scan.stop.strftime("%Yy%jd-%Hh%Mm%Ss"),px=ttl_ms)
                #-- set other parameters
                break

            if not n_scans_left:
                logger.debug("no more scans left, breaking")
                break

            logger.debug("going to sleep for %.3f seconds", wait)
            self.stopped.wait(wait)

        #-- cleanup, remove all keys that belong to schedule
        # Do this in two steps:
        #   1) get a list of associated keys
        #   2) only if non-empty list, then attempt delete
        # The check for non-empty list is required, since redis-call will raise
        # a ResponseError if an empty list is given to the delete command. Note
        # that redis.delete completes without error even if all the keys returned
        # by redis.keys have been removed before redis.delete is called (in this
        # case the delete call will just return zero)
        delete_list = self.redis.keys(self.prefix + "*")
        if delete_list:
            self.redis.delete(*delete_list)

    @property
    def is_stopped(self):
        return self.stopped.is_set()

    def set_stopped(self):
        """Set thread in stopped state.

        The thread's run method will return within one iteration of the main loop."""
        self.stopped.set()
        logger.debug("schedule redis reporter has been stopped (md5sum '%s')", self.name)

    MD5SUM_SHORT = 7
#}}}

class Mark6Data(object):
    """Implements Mark6-specific data acquisition"""
    def __init__(self, name, config):#{{{
        """Instantiate a Mark6 data acquisition object.

        Arguments:
        name -- full device name in config file
        config -- vlbicontrol/config.Config object from config file
        """
        self.name = name
        self.device = becs.config.get_mark6_inst(name, config, becs.mark6.Mark6)
        self.configfile = config

        #-- create queue to put dplane status messages (can store up to ~1h's worth of messages)
        self.dpsm_queue = queue.Queue(maxsize=900)

        #-- stream ids, for associating spooling flag with group name and data chain label
        self.stream_groupids = [[]] * len(config.devices[name]['inports'])
        self.device_outports = [po.split(config.delim_port)[1] for po in config.devices[self.name]['outports']]

        #-- state variables to moderate possibly disruptive Mark6 interaction
        self.cplane_wait = 15*60 # cplane only every 15 minutes
        self.cplane_after = time.time()
        self.processed_gap = False

        logging.getLogger("becs.mark6").setLevel(logging.WARN)

    def get_addr_queue(self):
        """Return IP address of device hostname and queue object in a tuple"""
        return (socket.gethostbyname(self.device.host), self.dpsm_queue)

    def collect_and_update(self, export_params):
        """Collect data points and add to given parameter dictionary"""
        config = self.configfile
        params = {}

        now = time.time()

        #-- do not collect parameters that impact on recording when scan in progress
        if scan_in_progress():
            #-- reset the processed gap flag
            self.processed_gap = False
        else:
            #-- if in gap, do gap-specific checks
            if not self.processed_gap and gap_in_progress():
                logger.debug("In unprocessed gap, collecting gap-specific info")
                self.processed_gap = True
                params.update(self._collect_in_gap(now))
            #-- check if cplane timer wait
            if now > self.cplane_after:
                self.cplane_after = now + self.cplane_wait
                logger.debug("Not in scan and cplane_after has passed, next cplane_after set to %.3f" % self.cplane_after)
                params.update(self._collect_outside_scan(now))

        #-- process dplane status message queue
        while True:
            try:
                msg = self.dpsm_queue.get(block=False)
            except queue.Empty:
                # no more messages to process
                break

            for n, stream in enumerate(msg.streams):
                t = msg.twhen if not stream.spooling else vdif.VDIFTime(stream.t_epoch_recent, stream.t_vdif_recent).to_datetime().timestamp()
                #-- more streams than inports
                if n >= len(self.stream_groupids):
                    logger.error('Number of streams defined > Number of input ports: ' + repr(msg.streams))
                    continue
                for p in self.stream_groupids[n]:
                    parname = '%sSpooling' % p
                    params[parname] = [[t, stream.spooling]]
            #-- report other groups as unconnected
            for p in self.device_outports:
                parname = '%sSpooling' % p
                if parname in params: continue
                params[parname] = [[t, False]]

        #-- report datachain labels for each group
        for c,chain in config.chains.items():
            l = chain[-1]
            pi = l.split(config.delim_link)[-1]
            for po in config.ports[pi]['feeds']:
                d, p = po.split(config.delim_port)
                #-- not this device
                if d != self.name: continue
                #-- port can only be connected to one chain
                parname = '%sChainlabel' % p
                assert parname not in params
                params[parname] = [[now, c]]
        #-- report other groups as unconnected
        for p in self.device_outports:
            parname = '%sChainlabel' % p
            if parname in params: continue
            params[parname] = [[now, '']]

        #-- update export parameters
        export_params.update(self._prefix_params(params))

    def _collect_outside_scan(self, when):
        params = {}

        #-- only report status and stop for unreachable device
        if not self.device.test_device_reachable():
            params["status"] = [[when, "unreachable"]]
            return params
        #-- report status as (un)configured
        params["status"] = [[when, "configured" if self.device.is_configured() else "unconfigured"]]

        params.update(self._collect_stream_params(when))
        params.update(self._collect_module_params(when))

        #-- disk space / recording time remaining
        usage = self.device.get_subgroup_usage()
        record_capacity = self.device.get_subgroup_record_capacity()
        for grp, mods in self.configfile.devices[self.name]['modules'].items():
            subgrp = ''.join([s.split('mod')[-1] for s in mods])
            params['%sSpaceLeft' % grp] = [when, usage[subgrp]['available']/1e9]
            params['%sTimeLeft' % grp] = [when, record_capacity[subgrp]['available']]

        #-- serial number
        params["serialNumber"] = self.device.serial

        return params

    def _collect_in_gap(self, when):
        params = {}

        #-- scan check
        sc_result = self.device.cp_scan_check()
        #-- result or keys might be empty
        try:
            #-- timestamp at exactly the end of the scan
            start = datetime.datetime.strptime(sc_result["streams"][0]["start"],"%Yy%jd%Hh%Mm%Ss")
            duration = datetime.timedelta(seconds=float(sc_result["streams"][0]["duration"]))
            sc_when = (start + duration).timestamp()
            params["scancheckId"] = [[sc_when, sc_result["scan_label"]]]
            #-- aggregate data rate across streams in MB/s
            params["scancheckDatarate"] = [[sc_when, int(sum([float(s['data_rate'])*1000 for s in sc_result["streams"]]))]]
            #-- aggregate missing bytes across streams in B
            params["scancheckBytesMissing"] = [[sc_when, sum([int(s['missing_bytes']) for s in sc_result["streams"]])]]
            #-- concatenate status strings across streams
            params["scancheckStatus"] = [[sc_when, '_'.join([s['status'] for s in sc_result["streams"]])]]
        except KeyError as ke:
            logger.error("KeyError raised, no key '%s' in scan_check result" % ke)
        except IndexError as ie:
            logger.error("IndexError raised, zero streams in scan_check result")

        #-- disk space / recording time remaining
        usage = self.device.get_subgroup_usage()
        record_capacity = self.device.get_subgroup_record_capacity()
        for grp, mods in self.configfile.devices[self.name]['modules'].items():
            subgrp = ''.join([s.split('mod')[-1] for s in mods])
            params['%sSpaceLeft' % grp] = [when, usage[subgrp]['available']/1e9]
            params['%sTimeLeft' % grp] = [when, record_capacity[subgrp]['available']]

        return params

    def _collect_stream_params(self, when):
        config = self.configfile
        params = {}

        # C-plane parameters
        try:
            #-- the stream order corresponds to that received in dplane status messages
            streams = self.device.cp_input_stream_query()
        except becs.mark6.VSIError as vsie:
            logger.error("could not get Mark6 input streams: %s", str(vsie))
            return params

        assert len(streams) <= len(self.stream_groupids)

        #-- stream ids, for associating spooling flag with group name and data chain label
        self.stream_groupids = [[]] * len(self.stream_groupids)
        for n, stream in enumerate(streams):
            pi = self.name + config.delim_port + stream["interface_ID"]
            a = []
            for po in config.ports[pi]['feeds']:
                groupid = po.split(config.delim_port)[-1]
                a.append(groupid)
            self.stream_groupids[n] = a

        #-- stream ids, for associating spooling flag with group name and data chain label
        for n, stream in enumerate(streams):
            val = ','.join(stream['sub_group'])
            for p in self.stream_groupids[n]:
                parname = '%sModules' % p
                assert parname not in params
                params[parname] = [[when, val]]
        #-- report other groups as unconnected
        for p in self.device_outports:
            parname = '%sModules' % p
            if parname in params: continue
            params[parname] = [[when, '']]

        return params

    def _collect_module_params(self, when):
        params = {}
        for mod in range(1,5):
            try:
                params["module%dID" % mod] = [[when, self.device.cp_mstat(mod)["MSN"]]]
            except becs.mark6.VSIError as vsie:
                logger.error("could not get Mark6 module status: %s", str(vsie))
        return params

    def _prefix_params(self, params_in):
        params_out = {}
        for k, v in list(params_in.items()):
            params_out[".".join((self.device.host,k))] = params_in[k]
        return params_out
#}}}

class DPlaneStatusMessageReceiver(threading.Thread):
    """Thread used to receive dplane status messages"""
    def __init__(self, *args, **kwargs):#{{{
        super(DPlaneStatusMessageReceiver, self).__init__(*args, **kwargs)
        #-- initialise stop condition
        self._stopped = threading.Lock()
        #-- initialise mapping of IP address to message buffer
        self._dpsm_queue_map = {}
        #-- initialise socket
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self._socket.settimeout(self.TIMEOUT)
        self._socket.bind((self.BROADCAST_ADDRESS, self.PORT))
        #-- set up redis
        self._redis = redis.Redis()

    def run(self):
        while True:

            #-- exit if stopped
            if self.is_stopped:
                break

            #-- receive messages
            try:
                binary, txsock = self._socket.recvfrom(4096)
            except socket.timeout:
                # just continue to next iteration on timeouts
                continue
            txaddr, txport = txsock
            #-- ignore packets from unkown addresses
            if txaddr not in self._dpsm_queue_map.keys():
                continue
            #-- interpret bytes as dplane status message
            try:
                dpsm = DPlaneStatusMessage(binary)
                logger.debug("received dplane status message %d from %s at %s", dpsm.seqno, txaddr, datetime.datetime.fromtimestamp(dpsm.twhen).strftime("%Y-%m-%d %H:%M:%S"))
                self._dpsm_queue_map[txaddr].put(dpsm)
                self._report_dpsm_to_redis(txaddr, binary)
            except AssertionError:
                # not dplane status message, continue with next iteration
                continue
            except queue.Full:
                logger.warn("dpsm queue full for %s", txaddr)

        #-- close socket
        self._socket.close()

    def add_dpsm_queue_mapping(self, address, queue):
        self._dpsm_queue_map[address] = queue

    def clr_dpsm_queue_mapping(self):
        self._dpsm_queue_map = {}

    @property
    def is_stopped(self):
        return self._stopped.locked()

    def set_stopped(self):
        """Set thread in stopped state.

        The thread's run method will return within one iteration of the main loop."""
        self._stopped.acquire()
        logger.debug("dplane status message receiver has been stopped")

    def _report_dpsm_to_redis(self, txaddr, dpsm_binary):
        host, aliaslist, ipaddrlist = socket.gethostbyaddr(txaddr)
        self._redis.set(".".join([host,"dstat"]), dpsm_binary)

    PORT = 7214
    BROADCAST_ADDRESS = "192.168.0.255"
    TIMEOUT = 30
#}}}

class DPlaneStatusMessage(object):
    """Represents a D-plane status message structure"""
    def __init__(self, binary):#{{{
        """Create D-plane status message from bytes"""
        # messages have fixed length of this size
        assert len(binary) == 248
        # message type, sequence number
        self.code, self.seqno = struct.unpack("<II",binary[:8])
        assert self.code == self.STATUS_MESSAGE
        # message timestamp in UNIX time
        self.twhen, = struct.unpack("<d", binary[8:16])
        hostname_bin = binary[16:36]
        self.hostname = "".join([chr(b) for b in hostname_bin[:hostname_bin.index(0)]])
        # status word, fileno associated with set GSW_FOPEN_ERR, number of active streams
        self.gsw, self.gsw_fileno, self.nstreams = struct.unpack("<iii", binary[36:48])
        # amount of data in cache, file timeout bits (bit n means timeout in file n)
        self.mb_cache, self.file_timeout = struct.unpack("<ii", binary[224:232])
        self.streams = []
        for i in range(4):
            stream = self.DplaneStreamStatus(binary[48+i*48:48+(i+1)*48])
            stream.set_spooling(self.gsw & (self.GSW_STR0_SPOOLING<<i))
            stream.set_sequence_err(self.gsw & (self.GSW_STR0_SEQUENCE_ERR<<i))
            self.streams.append(stream)

    class DplaneStreamStatus(object):
        """Represents a D-plane stream status structure"""
        def __init__(self, binary):#{{{
            """Create D-plane stream status message from bytes"""
            # amount of data in stream's ring buffer, last-read vdif sec, ..and epoch
            self.mb_ring, self.t_vdif_recent, self.t_epoch_recent = struct.unpack("<iii", binary[:12])
            # usable packets, wrong psn packets, wrong len packets, total bytes --- all since stream configure
            self.npack_good, self.npack_wrong_psn, self.npack_wrong_len, self.nbytes = struct.unpack("<qqqq", binary[12:44])

        @property
        def spooling(self):
            return self._spooling

        def set_spooling(self, w):
            self._spooling = bool(w)

        @property
        def sequence_err(self):
            return self._sequence_err

        def set_sequence_err(self, w):
            self._sequence_err = bool(w)
#}}}

    STATUS_MESSAGE = 8

    # GSW status bits
    GSW_FACTIVE_ERR        = 0x00000001
    GSW_FOPEN_ERR          = 0x00000002
    GSW_NO_STREAMS_ERR     = 0x00000004
    GSW_ACTIVE_STREAMS_ERR = 0x00000008
    GSW_BUFFER_OVERRUN_ERR = 0x00000010
    GSW_WRITE_ERROR        = 0x00000020
    GSW_M5PKT_SEQUENCE_ERR = 0x00000040
    GSW_NO_FILES_ERR       = 0x00000080
    GSW_STR0_SEQUENCE_ERR  = 0x00000100
    GSW_STR1_SEQUENCE_ERR  = 0x00000200
    GSW_STR2_SEQUENCE_ERR  = 0x00000400
    GSW_STR3_SEQUENCE_ERR  = 0x00000800
    GSW_STR0_SPOOLING      = 0x00001000
    GSW_STR1_SPOOLING      = 0x00002000
    GSW_STR2_SPOOLING      = 0x00004000
    GSW_STR3_SPOOLING      = 0x00008000
    GSW_FILES_OPEN         = 0x00010000
    GSW_PFRING_OPEN_ERR    = 0x00020000

#}}}
# vim: foldmethod=marker
